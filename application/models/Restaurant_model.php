<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Restaurant_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
    function get_all_restaurant()
    {
        $this -> db -> select('*');
        $this -> db -> from('restaurant');
        $query = $this -> db -> get();
        if($query -> num_rows() >= 1)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    function get_restaurant($data)
    {
        $this -> db -> select('*');
        $this -> db -> from('restaurant');
        $this -> db -> where('id',$data['id']);
        $query = $this -> db -> get();
        if($query)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    function lastest_restaurant()
    {
        $this -> db -> select('*');
        $this -> db -> from('restaurant');
        $this -> db -> order_by('id',$direction='DESC');
        $this -> db -> limit(3);
        $query = $this -> db -> get();
        if($query -> num_rows() >= 1)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
}
