<?php
Class Restaurant extends CI_Model
{
  function new_restaurant($data)
 {
    $result =  $this -> db -> set('Name',$data['name'])
                     -> set('pic',$data['pic'])
                     -> set('type',$data['type'])
                     -> set('location',$data['location'])
                     -> set('description',$data['description'])
                     -> set('short_description',$data['short_description'])
                     -> set('score',$data['score'])
                     ->insert('restaurant');
   if($result)
   {
     return $result;
   }
   else
   {
     return false;
   }
 }
 function edit_restaurant($data)
 {

 }

 function get_all_restaurant()
 {
    $this -> db -> select('*');
    $this -> db -> from('restaurant');
    $query = $this -> db -> get();
    if($query -> num_rows() >= 1)
   {
     return $query->result_array();
   }
   else
   {
     return false;
   }
 }

 function get_restaurant($data)
 {
    $this -> db -> select('*');
    $this -> db -> from('restaurant');
    $this -> db -> where('id',$data['id']);
    $query = $this -> db -> get();
    if($query)
   {
     return $query->result_array();
   }
   else
   {
     return false;
   }
 }

}
?>