<?php
Class Recipe extends CI_Model
{
    function new_recipe($data)
    {
        $result =  $this -> db -> set('Name',$data['name'])
            -> set('pic',$data['pic'])
            -> set('description',$data['description'])
            ->insert('recipe');
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    function edit_recipe($data)
    {

    }

    function get_all_recipe()
    {
        $this -> db -> select('*');
        $this -> db -> from('recipe');
        $query = $this -> db -> get();
        if($query -> num_rows() >= 1)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    function get_recipe($data)
    {
        $this -> db -> select('*');
        $this -> db -> from('recipe');
        $this -> db -> where('id',$data['id']);
        $query = $this -> db -> get();
        if($query)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

}
?>