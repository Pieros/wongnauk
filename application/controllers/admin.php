<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    /**
     * Index Page for this controller.     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->model('restaurant','',TRUE);
        $this->load->model('recipe','',TRUE);

    }
    public function index()
    {
        if(!$this->session->userdata('logged_in'))
        {
            $data['logout']='';
            $this->load->helper(array('form'));
            $this->load->view('admin/header',$data);
            $this->load->view('admin/login');
            $this->load->view('footer');
        }
        else{
            redirect('admin/restaurant', 'refresh');
        }
    }

    public function restaurant()
    {
        if($this->session->userdata('logged_in'))
        {
            $data['logout']='<li><a href="/wongnauk/admin/logout">logout</a></li>';
            $result = $this->restaurant->get_all_restaurant();
            $this->load->helper(array('form'));
            $this->load->view('admin/header',$data);
            $this->load->view('admin/type');

            if($result){
                foreach ($result as $row) {
                    $this->load->view('admin/show',$row);  
                }
            }

            $this->load->view('admin/footer');
        }
        else
        {
            //If no session, redirect to login page
            redirect('admin', 'refresh');
        }
    }

    public function recipe()
    {
        if($this->session->userdata('logged_in'))
        {
            $data['logout']='<li><a href="/wongnauk/admin/logout">logout</a></li>';
            $result = $this->recipe->get_all_recipe();
            $this->load->helper(array('form'));
            $this->load->view('admin/header',$data);
            $this->load->view('admin/type_recipe');

            if($result){
                foreach ($result as $row) {
                    $this->load->view('admin/show_recipe',$row);
                }
            }

            $this->load->view('admin/footer');
        }
        else
        {
            //If no session, redirect to login page
            redirect('admin', 'refresh');
        }
    }

    public function logout()
    {
       $this->session->unset_userdata('logged_in');
       session_destroy();
       redirect('admin', 'refresh');
    }

    public function add()
    {
        $this->load->view('admin/add');
    }

    public function addrecipe()
    {
        $this->load->view('admin/addrecipe');
    }

    public function new_restaurant()
    {
        $config = array(
        'upload_path' => "./public/images/uploads/",
        'allowed_types' => "gif|jpg|png|jpeg|pdf",
        'overwrite' => TRUE,
        'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
        'max_height' => "768",
        'max_width' => "1024"
        );
        $this->load->library('upload', $config);
        if($this->upload->do_upload())
        {   
            $upload_pic = $this->upload->data();
        }
        $submit=$this->input->post(NULL); 
        $path=explode( 'wongnauk', $upload_pic['full_path']);
        $submit['pic'] = '/wongnauk'.$path[1];
        $result = $this->restaurant->new_restaurant($submit);
        redirect('admin/restaurant', 'refresh');
    }

    public function new_recipe()
    {
        $config = array(
            'upload_path' => "./public/images/uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "768",
            'max_width' => "1024"
        );
        $this->load->library('upload', $config);
        if($this->upload->do_upload())
        {
            $upload_pic = $this->upload->data();
        }
        $submit=$this->input->post(NULL);
        $path=explode( 'wongnauk', $upload_pic['full_path']);
        $submit['pic'] = '/wongnauk'.$path[1];
        $result = $this->recipe->new_recipe($submit);
        redirect('admin/recipes', 'refresh');
    }

    public function edit()
    {
        $submit=$this->input->get(NULL); 
        $result = $this->restaurant->get_restaurant($submit);
        $this->load->view('admin/edit',$result[0]);
    }

    public function edit_recipe()
    {
        $submit=$this->input->get(NULL);
        $result = $this->recipe->get_recipe($submit);
        $this->load->view('admin/edit_recipe',$result[0]);
    }
}
