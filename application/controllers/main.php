<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->database();
        $this->load->model('Recipe_model','',TRUE);
        $this->load->model('Restaurant_model','',TRUE);
    }
    public function index()
    {
        $result =array('data' => $this->Recipe_model->lastest_recipe());
        $result1 =array('data' => $this->Restaurant_model->lastest_restaurant());
        $this->load->view('header');
        $this->load->view('home/slideshow');
        $this->load->view('home/restaurant',$result1);
        $this->load->view('home/recipeshow',$result);
        $this->load->view('footer');



    }
    public function type()
    {
        $result = $this->Restaurant_model->get_all_restaurant();
        $this->load->view('header');
        $this->load->view('type/type');
        if($result){
            foreach ($result as $row) {
                $this->load->view('type/restaurant-content',$row);
            }
        }
        $this->load->view('footer');
    }
    public function recipe()
    {
        $result = $this->Recipe_model->get_all_recipe();
        $this->load->view('header');
        $this->load->view('recipe/recipes');
        if($result){
            foreach ($result as $row) {
                $this->load->view('recipe/recipe-content',$row);
            }
        }
        $this->load->view('footer');

    }

    public function about()
    {
        $this->load->view('header');
        $this->load->view('about/aboutus');
        $this->load->view('footer');
    }

    public function detail()
    {
        $data =$this->input->get(NULL);
        $this->load->model('Recipe_model');
        $result = $this->Recipe_model->get_recipe($data);
        $this->load->view('recipe/detail',$result[0] );
    }

    public function detail_rest()
    {
        $data =$this->input->get(NULL);
        $this->load->model('Restaurant_model');
        $result = $this->Restaurant_model->get_restaurant($data);
        $this->load->view('type/detail_rest',$result[0] );
    }
}
