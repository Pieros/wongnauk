<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Home | Wongnauk</title>

    <!-- Bootstrap -->
    <link href="/wongnauk/public/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/wongnauk/public/css/carousel.css" rel="stylesheet">
    <link href="/wongnauk/public/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="/wongnauk/public/js/jquery.js" type="text/javascript"></script>
    <link href="/wongnauk/public/css/facebox.css" media="screen" rel="stylesheet" type="text/css"/>
    <script src="/wongnauk/public/js/facebox.js" type="text/javascript"></script>
    <script>
      jQuery(document).ready(function($) {
        $('a[rel*=facebox]').facebox({
          loadingImage : '/wongnauk/public/images/loading.gif',
          closeImage   : '/wongnauk/public/images/closelabel.png'
        })
      })
    </script>

  </head>
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo base_url();?>">Wongnauk</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <!-- <li class="active"><a href="#">Home</a></li> -->
                    <li><a href="<?php echo base_url();?>">Home</a></li>
                    <li><a href="<?php echo base_url('main/recipe');?>">Recipes</a></li>
                    <li><a href="<?php echo base_url('main/type');?>">New Reviews</a></li>
                    <li><a href="<?php echo base_url('main/about');?>">About us</a></li>
                </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>

 