<div class="container marketing">
  <h2 class="head-title">Restaurant</h2>
  <!-- Three columns of text below the carousel -->
  <div class="row">
    <?php foreach($data as $value){?>
      <div class="col-lg-4 card">
        <img class="img-circle" src=<?=$value['pic'];?>  width="140" height="140">
        <h4><?=$value['Name']?></h4>
        <p><?=$value['short_description']?></p>
        <p><a class="btn btn-default" href=<?="main/detail_rest?id=".$value['id'];?> rel="facebox" role="button">View details &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
    <?php } ?>

  </div><!-- /.row -->
</div>