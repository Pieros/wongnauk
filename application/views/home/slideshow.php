<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="item active" id="slide1">
      <!-- <img class="first-slide" src="" alt="First slide"> -->
      <div class="container">
        <div class="carousel-caption">
          <h1>New Reviews</h1>
          <p>พบกับreviewsร้านอาหารใหม่ๆได้ที่Wongnauk</p>
<!--          <p><a class="btn btn-lg btn-primary" href="#" role="button">View more</a></p>-->
        </div>
      </div>
    </div>
    <div class="item" id="slide2">
      <!-- <img class="second-slide" src="/wongnauk/public/images/recipe1.PNG" alt="Second slide"> -->
      <div class="container">
        <div class="carousel-caption">
          <h1>Recipes</h1>
          <p>พบกับสูตรอาหารนานาชนิด ทั้งอาหารไทยและอาหารต่างประเทศได้ที่Wongnauk</p>
<!--          <p><a class="btn btn-lg btn-primary" href="#" role="button">View more</a></p>-->
        </div>
      </div>
    </div>
    <div class="item" id="slide3">
      <!-- <img class="third-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Third slide"> -->
      <div class="container">
        <div class="carousel-caption">
          <h1>Happy eating with Wongnauk</h1>
          <p>วงนอกจะพาคุณไปพบกับการรีวิวร้านอาหารในย่านมหาวิทยาลัยธรรมศาสตร และสูตรอาหารหลากหลายสำหรับคนรักอาหาร</p>
<!--          <p><a class="btn btn-lg btn-primary" href="#" role="button">View more</a></p>-->
        </div>
      </div>
    </div>
  </div>
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div><!-- /.carousel -->