<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<div class="form-add">
    <?php echo validation_errors();?>
    <?php echo form_open_multipart('admin/new_recipe');?>
    <h3><label>Name*</label></h3>
    <?php
    { $data = array(
        'name'  => 'name',
        'id'    => 'name',
        'size'  => '90',
        'required'=>''
    );
        echo form_input($data);
    }
    ?>
    <h3><label>Picture</label></h3>
    <?php
    { $data = array(
        'type'  => 'file',
        'name'  => 'userfile',
        'id'    => 'userfile',
        'size'  => '90',
        'required'=>'',
    );
        echo form_input($data);
    }
    ?>
    <div class="preview">
        <img src="" hidden></img>
    </div>
    <script>
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.preview img').show();
                    $('.preview img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input[name*='userfile']").change(function(){
            readURL(this);
        });
    </script>
    <h3><label>Description*</label></h3>
    <?php
    { $data = array(
        'name'  => 'description',
        'id'    => 'description',
        'rows'  => '10',
        'cols'  => '90',
        'required'=>''
    );
        echo form_textarea($data);
    }
    ?>

    <input type="submit" value="Submit" rel="facebox">
    <?php echo form_close(); ?>
</div>
</body>
</html>