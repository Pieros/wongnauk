<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/wongnauk/public/css/sheet.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/wongnauk/public/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <link href="/wongnauk/public/css/custom.css" rel="stylesheet">
    <script src="/wongnauk/public/js/jquery.js" type="text/javascript"></script>
    <link href="/wongnauk/public/css/facebox.css" media="screen" rel="stylesheet" type="text/css"/>
    <script src="/wongnauk/public/js/facebox.js" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function($) {
            $('[rel*=facebox]').facebox({
                loadingImage : '/wongnauk/public/images/loading.gif',
                closeImage   : '/wongnauk/public/images/closelabel.png'
            })
        })
    </script>

    <title>Welcome to Wongnauk</title>
</head>
<body>
    <header>
        <a href="<?php echo base_url();?>" class="banner-admin">
            <img src = "/wongnauk/public/images/logo.jpg" alt="logo">
        </a>
        <nav class="ridge">
            <ul>
                <li><a href="/wongnauk/admin/restaurant">Restaurant</a></li>
                <li><a href="/wongnauk/admin/recipe">Recipes</a></li>
                <?php
                    echo $logout; 
                ?>
            </ul>
        </nav>
    </header>