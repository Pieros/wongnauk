<div class="container content ">
    <div class="banner">
        <img src="/wongnauk/public/images/Real-Recipe-Plans.png">
        <div class="description">
            <h3>SIMPLE, DELICIOUS, AND NUTRITIOUS RECIPES.</h3>
        </div>
    </div>
    <div class="string">
        <div class="row">
            <div class="col-md-4">
                <a href="#" class="thumbnail">
                    <img src="/wongnauk/public/images/tomyumgoong.jpg" alt="thai food" style="width:250px;height:200px">
                    <p><?=$notify_recipe;?></p>
                </a>
            </div>
            <div class="col-md-4">
                <a href="#" class="thumbnail">
                    <img src="/wongnauk/public/images/jfood.jpg" alt="japanese food" style="width:250px;height:200px">
                    <p><?=$notify_recipe;?></p>
                </a>
            </div>
            <div class="col-md-4">
                <a href="#" class="thumbnail">
                    <img src="/wongnauk/public/images/italian.jpg" alt="italian food" style="width:250px;height:200px">
                    <p><?=$notify_recipe;?></p>
                </a>
            </div>
            <div class="col-md-4">
                <a href="#" class="thumbnail">
                    <img src="/wongnauk/public/images/dessert.jpg" alt="dessert" style="width:250px;height:200px">
                    <p><?=$notify_recipe;?></p>
                </a>
            </div>
        </div>
    </div>
</div>

