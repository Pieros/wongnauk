-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- โฮสต์: 127.0.0.1
-- เวลาในการสร้าง: 
-- เวอร์ชั่นของเซิร์ฟเวอร์: 5.5.34
-- รุ่นของ PHP: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- ฐานข้อมูล: `sanji_address`
--

-- --------------------------------------------------------

--
-- โครงสร้างตาราง `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` int(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- dump ตาราง `admin`
--

INSERT INTO `admin` (`id`, `name`, `password`) VALUES
(1, 'admin', 21232),
(2, 'admin', 0);

-- --------------------------------------------------------

--
-- โครงสร้างตาราง `recipe`
--

CREATE TABLE IF NOT EXISTS `recipe` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `Name` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pic` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- dump ตาราง `recipe`
--

INSERT INTO `recipe` (`id`, `Name`, `description`, `pic`) VALUES
(1, 'Taco-Stuffed Peppers Recipe', '<pre>Here is what you need:\r\n6-8 Medium/Large Bell Peppers - ensure they have a flat base so they will stand up when cooking\r\n1 lb Ground Beef\r\n1 Medium Yellow Onion - Diced\r\n1 Can Black Beans - Drained\r\n1 Cup Cooked Brown Rice\r\n1 Cup Frozen Corn\r\n1 Jar Medium Chunky Salsa\r\n1 Packet (about 2 Tbsp.) of Taco Seasoning Mix*\r\n1 1/2 - 2 Cups Grated Cheddar/Mexican Blend Cheese \r\n\r\nOptional: Guacamole and Sour Cream to finish\r\n\r\n*To make your own Taco Seasoning at home: (note: makes more than needed for this recipe)\r\n1 Tbsp. Chili Powder\r\n1 1/2 tsp Ground Cumin\r\n1 tsp Salt\r\n1 tsp Black Pepper\r\n1 tsp Corn Starch\r\n1/2 tsp Paprika\r\n1/2 tsp Garlic Powder\r\n1/2 tsp Onion Powder\r\n1/4 tsp Cayenne Pepper\r\n1/4 tsp Oregano\r\nDirections:\r\nCut off the top and remove the seeds from each pepper. Place in a baking dish and bake for 20 minutes in a preheated oven at 350˚F (175˚C). \r\n\r\nWhile the Peppers are roasting - heat oil in a large skillet over medium/high heat. Add ground beef and season with entire taco seasoning packet. Allow beef to brown on all sides. Add diced onion - continuing to cook until softened. Reduce heat to Medium. Mix in the black beans, brown rice, corn, and salsa. Add in 1 cup of cheese and stir until incorporated and the cheese has melted. Remove from heat. \r\n\r\nRemove peppers from oven and fill each with the taco mixture. Top with additional cheese and return to the oven for 15 minutes until the cheese has melted. Top each pepper with a generous scoop of guacamole and sour cream. Enjoy!</pre>\r\n ', '/wongnauk/public/images/recipe/1.jpg'),
(2, 'Grilled Cheese And Tomato Soup Bread Bowl Recipe', '<pre>Here is what you need:\r\nRound Bread Roll\r\nCheese Slices\r\nButter\r\nTomato Soup \r\nHere is how to prepare the the bread bowl:\r\nRemove the top of the roll. Using a small knife cut around the inside of the bread (being careful not to cut through the sides) and scoop out the center of the roll - save this for later. \r\n\r\nButter the inside of the roll and place 3-4 cheese slices around the inner edge of the bread bowl. Bake in a preheated oven at 375˚F (190˚C) until the cheese has melted. \r\n\r\nCut the saved center of the roll in half. Butter both sides and create a cheese sandwich. Melt butter in a small skillet over medium heat and fry the sandwich until both sides are browned and the inner cheese has melted. \r\n\r\nRemove the bread bowl from the oven. Fill with tomato soup and serve with the grilled cheese. Enjoy!</pre>', '/wongnauk/public/images/recipe/2.jpg'),
(3, 'Dough Balls Bolognese Recipe', '<pre>Here is what you’ll need:\r\nbutter\r\n1/2 onion\r\n1 carrot\r\nsalt\r\n1 stick celery\r\n500g beef\r\n200ml red wine\r\n400g chopped tomatoes\r\n150 g fresh mozzarella\r\n25 - 30 garlic dough balls\r\ngarlic butter (should come with the dough balls)\r\nMethod:\r\nMelt a knob of butter in a cast iron skillet and add the diced onion. season with salt and cook until slightly brown.\r\n\r\nAdd the diced carrot and diced celery cooking for another few minutes until everything is softer.\r\n\r\nAdd the mince, breaking in up with a spoon and cook until browned, stirring occasionally.\r\n\r\nStir in 200ml red wine and the chopped tomatoes, cooking until the sauce has reduced.\r\n\r\nPat down the bolognese with the back of the wooden spoon.\r\n\r\nLayer slices of fresh mozzarella all over the bolognese and cook until slightly melted.\r\n\r\nLayer the dough balls on top of the mozzarella then, using a pastry brush or teaspoon, coat them with the garlic butter dip, you may need to melt this beforehand.\r\n\r\nBake in the centre of a preheated oven for 8-12 minutes at 200°C/400°F until the dough balls brown.</pre>\r\n', '/wongnauk/public/images/recipe/3.jpg'),
(4, 'Bean And Cheese Stuffed Mashed Potato Balls Recipe', '<pre>Here is what you need:\r\n2-3 potatoes\r\n50g flour\r\nSalt\r\nCan of beans\r\nCheese of choice\r\nOil to fry with\r\nDirections:\r\nBoil potatoes until soft. Strain and place in a large mixing bowl. Add salt to taste and mash. Add flour and mash until very smooth. Grab a handful of mashed potatoes and shape into a disk. Push a small indent into the middle and fill with some beans and cheese. Wrap mashed potato around the cheese and beens and work until fully incased in potato. Chill at least until cool or up to overnight. Fry in oil until golden brown.</pre>', '/wongnauk/public/images/recipe/4.jpg'),
(5, 'Cheesy Bacon Dip', '<pre>Ingredients:\r\n1 (8 oz.) package cream cheese, room temperature\r\n2 cups (16 oz) sour cream\r\n1 1/2 cups grated sharp cheddar cheese (plus 2 Tbsp to sprinkle on top)\r\n1 cup crumbled cooked bacon (6-8 slices)\r\n1/3 cup green onions, chopped\r\nInstructions:\r\n1. Preheat the oven to 375 F.\r\n2. In a medium-sized mixing bowl, mix all the ingredients until combined. Scrape into a 9-inch pie plate or baking pan, then sprinkle the additional 2 Tbsp cheddar on top.\r\n3. Bake in the preheated oven for 30-35 minutes, until the cheese is melted and brown on top.\r\n4. Serve with bagel chips, pita chips, toasted bread, carrot sticks, etc etc.</pre>', '/wongnauk/public/images/recipe/5.jpg');

-- --------------------------------------------------------

--
-- โครงสร้างตาราง `restaurant`
--

CREATE TABLE IF NOT EXISTS `restaurant` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `short_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `score` int(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`(191)),
  KEY `type_2` (`type`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=9 ;

--
-- dump ตาราง `restaurant`
--

INSERT INTO `restaurant` (`id`, `Name`, `pic`, `type`, `location`, `description`, `short_description`, `score`) VALUES
(1, 'The staek', '/wongnauk/public/images/rest/1.jpg', 'Italian', 'อำเภอเมืองปทุมธานี', 'ร้านthe steak เป็นร้านสเต็กที่มีขนาดเล็กๆ  เมนูสเต็กให้เลือกมากมายหลายอย่างโดยเนื้อสเต็กร้านนี้จะมีจุดเด่นที่ความนุ่มของเนื้อที่นุ่มมากกว่าหลายร้านที่เคยทานมา  อีกทั้งยังมีเมนูสปาเกตตี้  ของทานเล่น  และมีเมนูอาหารญี่ปุ่นจำพวกราเมง  ข้าวแกงกะหรี่  ให้ลูกค้าสามารถเลือกทานอาหารได้หลายแนวตามความต้องการ  ราคาก็ไม่แพงมาก อิ่มอร่อยสบายกระเป๋า', 'ร้านthe steak เป็นร้านสเต็กที่มีขนาดเล็กๆ  อยู่ที่ตัวเมืองปทุมธานี ที่มีเมนูหลากหลายให้ลูกค้าได้เลือกทาน', 8),
(2, 'Nomwan', '/wongnauk/public/images/rest/2.jpg', 'Dessert', 'เชียงราก ใต้หอทวินทาวน์', 'ร้านขนมหวานที่มีเมนูให้เลือกหลายอย่าง  โดยเมนูส่วนใหญ่จะทานกับไอศกรีมโดยมีรสไอศกรีมให้เลือกพอสมควร  ตัวอย่างเมนูเช่น ฮันนี่โทสต์  วาฟเฟิล  ปังเย็น  เครป บราวนี่  โดยเราสามารถเลือกทานกับไอศกรีมรสใดก็ได้ที่ร้านมี  บรรกาศภายในร้านค่อนข้างดี มีที่นั่งให้เยอะกว่าหลายๆร้านในแถวนั้น  ราคานักศึกษา', 'ร้านnomwan ร้านขนมหวานติดรั้วมหาวิทยาลัยธรรมศาสตร์', 7),
(3, 'Hour', '/wongnauk/public/images/rest/3.jpg', 'Dessert', 'ใต้หอinterpark', 'ร้านขนมหวานที่ตกแต่งร้านด้วยนาฬิกาเป็นส่วนใหญ่  บรรยากาศภายในร้านน่านั่ง  มีเมนูแปลกใหม่ให้เลือกค่อนข้างแตกต่างจากร้านอื่นๆ  รสชาติและปริมาณที่ได้ถือว่าคุ้มราคา  ราคาแต่ละเมนูไม่แพงมากนักศึกษาสามารถทานได้', 'ร้านขนมหวานเล็กๆบรรยากาศน่านั่ง', 8),
(4, '44degree', '/wongnauk/public/images/rest/4.jpg', 'Dessert', 'เชียงราก ใต้หอinterpark', 'ร้านไอศกรีมที่มีไอกรีมให้เลือกหลายรสโดยแต่ละรสทางร้านจะตั้งชื่อให้แปลกๆเป็นเอกลักษณ์ของร้านถ้าอ่านชื่อแล้วสงสัยว่าเป็นรสอะไรก็ถามพนักงานที่ร้านได้  เป็นร้านเล็กๆมีที่นั่งประมาณ4-5โต๊ะ  มีเมนูให้เลือกพอสมควร ส่วนราคาถือว่าค่อนข้างแพงกว่าร้านอื่นเล็กน้อย', 'ร้าน44degree ร้านขนมหวานติดรั้วมหาวิทยาลัยธรรมศาสตร์', 7),
(5, 'Poon Poon', '/wongnauk/public/images/rest/5.jpg', 'Italian', 'เชียงราก', 'ร้านอาหารอิตาเลียนขนาดเล็กๆมีที่นั่งไม่มากนัก ภายในร้านติดแอร์เย็นสบาย  มีเมนูมากมายให้เลือกทั้งของคาวของหวาน  ภายในร้านเป็นครัวเปิดระหว่างนั่งรอก็จะเห็นแม่ครัวปรุงอาหารได้  รสชาติและปริมาณอาหารที่ร้านนี้ถือว่าโอเคมากๆเทียบกับราคาที่ไม่แพงมาก แต่จะมีอาหารบางอย่างที่ได้น้อย เช่น พิซซ่าคนที่ทานเยอะมากๆไม่ควรจะสั่งเพราะว่าไม่อิ่มแน่นอน', 'ร้านอาหารสไตล์อิตาเลียน  บรรยากาศดี  อาหารอร่อย', 8),
(6, 'Shinkanzen', '/wongnauk/public/images/rest/6.jpg', 'Japanese', 'เชียงราก', 'ร้านอาหารญี่ปุ่นที่เน้นขายซูชิเป็นหลักมีเมนูอื่นๆบ้างเป็นพวกข้าวหน้าหมูย่าง  ไก่คาราอาเกะเป็นต้น  ร้านนี้จะมีคนมาทานเยอะมากโดยเฉพาะช่วงกลางวันกับตอนเย็น  อาจจะต้องมาจองคิวกันไว้ก่อน  ที่นั่งค่อนข้างแคบไปนิดเวลาคนเยอะจะค่อนข้างนั่งลำบาก  ซูชิที่ร้านนี้ถือว่าคุณภาพกับราคาถือดีที่สุดแล้วในแถวนั้น  ปลาแซลมอลทีนำมาทำซูชิโอเคกว่าทุกร้านในแถวนั้น  ราคาซูชิแต่ละคำก็มีตั้งแต่10บาท ขึ้นไป', 'ร้านซูชิshinkazen มหาวิทยาลัยธรรมศาสตร์', 8),
(7, 'Marugame Seimen', '/wongnauk/public/images/rest/7.jpg', 'Japanese', 'future park ', 'ร้านอุด้งที่สามารถเลือกเมนูอุด้ง และเครื่องเคียงอื่นๆได้ส่วนใหญ่เป็นพวกเทมปุระ เป็นร้านที่ให้ลูกค้าบริการตัวเอง ต้องถือถาดอาหารไปจ่ายเงินตอนจ่ายเงินพนักงานจะถามว่ารับน้ำอะไร  มีน้ำเก๊กฮวย ชาเขียว เป๊ปซี่ เป็นต้น แล้วพนักงานก็จะคิดเงิน พอจ่ายเงินเสร็จทานเสร็จแล้วก็ต้องเก็บจานเองโดยนำไปเก็บไว้ที่ชั้นที่ร้านเตรียมไว้', 'Marugame Seimen (มารุกาเมะ เซเมง) เป็นร้านอุด้งชื่อดังจากญี่ปุ่นที่มาเปิดสาขาที่เมืองไทยที่ Rain Hill โดยมาพร้อมกับคอนเซปต์ระบบการให้บริการตนเอง', 8),
(8, 'อรทัยซูชิ', '/wongnauk/public/images/rest/8.jpg', 'Japanese', 'วังหลัง', 'ร้านซูชิราคาไม่แพง  นักเรียนนักศึกษาที่ยังไม่มีรายได้สามารถทานได้  มีเมนูซูชิให้เลือกเยอะมากจะสั่งแยกเป็นชิ้นหรือสั่งเป็นเซตก็ได้\r\nคุณภาพอาหารก็พอทานได้ไม่ได้แย่มาก  ปริมาณก็ได้เยอะพอสมควร  ปกติร้านนี้คนจะเยอะโดยเฉพาะช่วงเที่ยงวัน\r\n', 'ร้านซูชิซูชิราคาประหยัดและเมนูชุดเซทปลาดิบราคาประหยัด ให้เลือกอร่อยกันได้อีกกว่า 30 เมนู ', 6);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
